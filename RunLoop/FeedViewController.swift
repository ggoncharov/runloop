//
//  FeedViewController.swift
//  RunLoop
//
//  Created by Gotlib on 12.06.2018.
//  Copyright © 2018 Gleb Goncharov. All rights reserved.
//

import UIKit

class FeedViewController: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var mTable: UITableView!
    @IBOutlet weak var loadingLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    /// Singletone instace
    static let sharedInstance = FeedViewController()
    
    var selectedFeed : String!
    var linkString : String!
    var selectedFeedLink: String!
    // entries and parser
    var parser: FeedParser?
    var entries: [FeedItem]?
    
    enum TableSection: Int {
        case entertainmentNews = 0, environmentNews, total
    }
    
    var sortedData = [TableSection: [FeedItem]]()
    
    func sortdata(){
        var entertainmentNews = [FeedItem]()
        var environmentNews = [FeedItem]()
        for data in entries!{
            if data.feedCategories[0] == "entertainmentNews" {
                entertainmentNews.append(data)
            }
            else if data.feedCategories[0] == "environmentNews" {
                 environmentNews.append(data)
            }
        }
        sortedData[.environmentNews] = environmentNews
        sortedData[.entertainmentNews] = entertainmentNews

    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.linkString = "http://feeds.reuters.com/reuters/businessNews"
        entries = []
        self.updateFeed()

        Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(updateFeed), userInfo: nil, repeats: true)
        // Do any additional setup after loading the view.
    }

    //Update feed
    @objc fileprivate func updateFeed() {

        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        self.entries?.removeAll()

        if self.segmentedControl.selectedSegmentIndex == 0  {
            DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async(execute: { () -> Void in
                self.parser = FeedParser(feedURL: "http://feeds.reuters.com/reuters/businessNews")
                self.parser?.delegate = self
                self.parser?.parse()
            })
        }
        else    {
            DispatchQueue.global(qos: DispatchQoS.QoSClass.default).sync(execute: { () -> Void in
                self.parser = FeedParser(feedURL: "http://feeds.reuters.com/reuters/entertainment")
                self.parser?.delegate = self
                self.parser?.parse()
            })
            DispatchQueue.global(qos: DispatchQoS.QoSClass.default).sync(execute: { () -> Void in
                self.parser = FeedParser(feedURL: "http://feeds.reuters.com/reuters/environment")
                self.parser?.delegate = self
                self.parser?.parse()
            })
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if segue.identifier == "segue" {
            
            if let detailViewController = segue.destination as? DetailsViewController{
                
                guard let current = self.selectedFeedLink else {return}
                detailViewController.detailsLink = current
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action
    @IBAction func segmentedControlAction(_ sender: UISegmentedControl) {

        self.updateFeed()
    }
}

// MARK: - FeedParserDelegate methods
extension FeedViewController: FeedParserDelegate {
    
    func feedParser(_ parser: FeedParser, didParseChannel channel: FeedChannel) {
        // Here you could react to the FeedParser identifying a feed channel.
        DispatchQueue.main.async(execute: { () -> Void in
            print("Feed parser did parse channel \(channel)")
        })
    }
    
    func feedParser(_ parser: FeedParser, didParseItem item: FeedItem) {
        DispatchQueue.main.async(execute: { () -> Void in
            self.entries?.append(item)
        })
    }
    
    func feedParser(_ parser: FeedParser, successfullyParsedURL url: String) {
        DispatchQueue.main.async(execute: { () -> Void in
            if ((self.entries?.count)! > 0) {
                print("All feeds parsed.")
                self.mTable.isHidden = false
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                self.sortdata()
                self.mTable.reloadData()
            } else {
                print("No feeds found at url \(url).")
                self.mTable.isHidden = true
                self.activityIndicator.isHidden = false
                self.activityIndicator.stopAnimating()
                self.loadingLabel.text = "No feeds found at url \(url)."
            }
        })
    }
    
    func feedParser(_ parser: FeedParser, parsingFailedReason reason: String) {
        DispatchQueue.main.async(execute: { () -> Void in
            print("Feed parsed failed: \(reason)")
            self.entries = []
            self.mTable.isHidden = true
            self.loadingLabel.text = "Failed to retrieve feeds from \(self.parser!.feedURL)"
        })
    }
    
    func feedParserParsingAborted(_ parser: FeedParser) {
        print("Feed parsing aborted by the user")
        self.entries = []
        self.mTable.isHidden = true
        self.loadingLabel.text = "Feed loading cancelled by the user."
    }

}

// MARK: - UITableViewDelegate, UITableViewDataSource methods
extension FeedViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        print(TableSection.total.rawValue)
        if self.segmentedControl.selectedSegmentIndex != 0  {
            return TableSection.total.rawValue
        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        //        return entries?.count ?? 0
        if self.segmentedControl.selectedSegmentIndex != 0  {
            if let tableSection = TableSection(rawValue: section), let data = self.sortedData[tableSection] {
                return data.count
            }
        }
        else{
            return entries?.count ?? 0
        }
        
        return entries?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        if let tableSection = TableSection(rawValue: section), let data = self.sortedData[tableSection], data.count > 0 {
            return 40
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 40))
        let label = UILabel(frame: CGRect(x: 15, y: 0, width: tableView.bounds.width - 30, height: 40))
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.textColor = UIColor.black
        
        if let tableSection = TableSection(rawValue: section) {
            switch tableSection {
            case .entertainmentNews:
                label.text = "Entertainment News"
            case .environmentNews:
                label.text = "Environment News"
            default:
                label.text = ""
            }
        }
        view.addSubview(label)

        return view
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedTableViewCell", for: indexPath) as! FeedTableViewCell

        if self.segmentedControl.selectedSegmentIndex != 0  {
            if let tableSection = TableSection(rawValue: indexPath.section), let data = sortedData[tableSection]?[indexPath.row] {
                cell.titleLable.text = data.feedTitle
                cell.desriptionLable.text = data.feedContentSnippet ?? data.feedContent?.stringByDecodingHTMLEntities() ?? ""
            }
            return cell
        }
        else {
            let item = entries![(indexPath as NSIndexPath).row]
            // title
            cell.titleLable.text = item.feedTitle ?? "Untitled feed"

            // subtitle
            cell.desriptionLable.text = item.feedContentSnippet ?? item.feedContent?.stringByDecodingHTMLEntities() ?? ""
            
            return cell
        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = entries![(indexPath as NSIndexPath).row]
        self.selectedFeedLink = item.feedLink
        FeedViewController.sharedInstance.selectedFeed = item.feedTitle ?? "Untitled feed"
        performSegue(withIdentifier: "segue", sender: self)

    }
    
}
