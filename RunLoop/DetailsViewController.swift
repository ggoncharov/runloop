//
//  DetailsViewController.swift
//  RunLoop
//
//  Created by Gotlib on 13.06.2018.
//  Copyright © 2018 Gleb Goncharov. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var detailsWebView: UIWebView!
    
    var detailsLink : String!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.detailsWebView.delegate = self
        
        guard let url = URL (string: self.detailsLink) else {return}
        let requestObj = URLRequest(url: url)
        detailsWebView.loadRequest(requestObj)
        self.activityIndicator.startAnimating()
        // Do any additional setup after loading the view.
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        self.activityIndicator.isHidden = true
        self.activityIndicator.stopAnimating()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
