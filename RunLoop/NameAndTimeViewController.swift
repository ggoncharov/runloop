//
//  NameAndTimeViewController.swift
//  RunLoop
//
//  Created by Gotlib on 12.06.2018.
//  Copyright © 2018 Gleb Goncharov. All rights reserved.
//

import UIKit

class NameAndTimeViewController: UIViewController {

    /// my name lable
    @IBOutlet weak var nameLable: UILabel!
    /// current time lable
    @IBOutlet weak var timeLable: UILabel!
    /// opened feed title
    @IBOutlet weak var feedNameLable: UILabel!
    
    
    var format = DateFormatter()
    
    override func viewDidLoad() {

        super.viewDidLoad()

        format.dateFormat = "yyyy/MM/dd hh:mm:ss"
        
        //Update the date and time of the clock periodically.
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateClock), userInfo: nil, repeats: true)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        self.feedNameLable.text = FeedViewController.sharedInstance.selectedFeed
    }
    
    //Update clock every second
    @objc fileprivate func updateClock() {
        let now = NSDate()
        self.timeLable.text = format.string(from: now as Date)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
